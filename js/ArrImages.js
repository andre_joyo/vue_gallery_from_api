var startTime=Date.now();
Vue.component('ArrImages', {
    template:   `<div>
                    <h2>an API</h2>
                    <p v-html="message"></p>
                    <h3>Images from an array of objects</h3>
                    <div v-for="(item, key, index) in urls" :key="index">
                        <img :src=item alt="A photo from API" class="imageFromAPI" >
                    </div>
                </div>`,
    data() {
        return {
            title: 'Directiva v-for',
            message: '<b>Scroll down to charge more photos</b>',
            urls: this.getImages()
        }
    },
    methods: {
        getImages() {
            var images=[];
            windowWidth=window.innerWidth;
            imgsPerRow=5;
            imgWidth=windowWidth/imgsPerRow;
            rows=Math.ceil(window.innerHeight/imgWidth);
            for (let i = 0; i < rows; i++) {
                for (let j = 1; j <= imgsPerRow; j++) {
                    var request = new XMLHttpRequest();
                    request.open('GET', 'http://localhost:3000/api/'+parseInt(i*imgsPerRow+j), false);  // `false` makes the request synchronous
                    request.send(null);   
                    if (request.status === 200) {   //console.log(request.responseText);
                        url=JSON.parse(request.responseText)['url'];
                        images.push(url);
                    }
                }
            }
            nextIndex=rows*5+1;
            this.detectScroll(rows);
            return images;
        },
        getResults() {
            var boundY=window.innerHeight;
            width=boundY/5;
            boundY=document.body.scrollHeight;  
            scrollLimit = document.body.scrollHeight - window.scrollY - window.innerHeight;
            //HERE CONTROL TIME INTERVAL
            if (scrollLimit<=0) {
                if(Date.now()-startTime>500){
                    startTime=Date.now();
                    imagesQuantity=this.urls.length;
                    for (let j = 1; j <= 5; j++) {
                        axios
                        .get(
                        'http://localhost:3000/api/'+parseInt(imagesQuantity+j)
                        )
                        .then(res => {
                            (this.urls).push(res.data.url);
                            //console.log(res.data); console.log(imagesQuantity);  quantity before rerender -> real Q - imgsPerRow
                        })
                        .catch(err => console.log(err));
                    }
                }
            }
        },
        detectScroll(rows) {
            window.onscroll = ev => {
                this.getResults(rows)
            };
        },
    },
    updated: function () {
        this.$nextTick(function () {
            setSize();  // Code that will run only after the entire view has been re-rendered
        })
      },
    mounted () {
        setSize();  // Code that will be executed only after rendering the full view
    }
})