# Vue gallery from API rest

Galería de imágenes hecha con Vue.js

## Empezando

Se necesita iniciar json server

### Prerequisitos

Conocer HTML, CSS, JS, Vue.js

### Instalación

Instalar Node.js para poder instalar json server
Instalar json server

## Construido con

* [Vue.js](http://www.https://vuejs.org/) - The web framework used
* [npm](https://www.npmjs.com/) - Dependency Management

## Versión

Versión única

## Autor

* **André Joyo**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details